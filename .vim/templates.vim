autocmd BufNewFile */th2/*/*.tex r ~/Templates/theoII.tex
autocmd BufNewFile */ex2/*/*.tex r ~/Templates/exII.tex
autocmd BufNewFile */ma2/*/*.tex r ~/Templates/mathII.tex
autocmd BufNewFile progdoc*.tex r ~/Templates/prog.tex
